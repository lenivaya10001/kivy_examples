import sys
import kivy
kivy.require('1.9.1')

from kivy.animation import Animation
from kivy.uix.image import Image
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.app import App
from kivy.properties import ObjectProperty, NumericProperty, StringProperty, ListProperty

from kivy.clock import Clock
from functools import partial

class TryAniFuncAni(FloatLayout):
    '''try change button size_hint then change image then again change button size_hint y, and recieve value from kv'''
    # def __init__(self, **kwargs):
        # super(TryAniFuncAni, self).__init__(**kwargs)
    try:
        absh = ListProperty()
        t = StringProperty()
        d = NumericProperty()
        s = NumericProperty()
        
    except:
        print(sys.exc_info())
        input()
    
    def imgs_change(self,imgself,*largs):
        if imgself.source == 'imgs/imgx/x.png':
                imgself.source = 'imgs/imgy/y.png'
    
    # def btn_pressed(self,btn,t_ans,d_ans,s_ans,absh):
        # """WORK DONE NOW TRY RECIEVE VALUES USE KIVY PROPERTY"""
        # try:
            # anim1 = Animation(size_hint=(absh[0],absh[1]/2), t=t_ans, d=d_ans, s=s_ans) 
            # anim1 += Animation(size_hint=(absh[0],absh[1]), t=t_ans, d=d_ans, s=s_ans)
            # anim1.start(btn)
            # Clock.schedule_once(partial(self.imgs_change,btn.children[0]),0.5) #work done
            # print("DONE")
        # except:
            # print(sys.exc_info())
            # input()
    
    def btn_pressed(self,btn):
        """USE KIVY PROPERTY"""
        try:
            print('51 py self.absh[1] = ' + str(self.absh[1]))
            anim1 = Animation(size_hint=(self.absh[0],self.absh[1]/2), t=self.t, d=self.d, s=self.s) 
            anim1 += Animation(size_hint=(self.absh[0],self.absh[1]), t=self.t, d=self.d, s=self.s)
            anim1.start(btn)
            Clock.schedule_once(partial(self.imgs_change,btn.children[0]),0.5) #work done
            print("DONE")
        except:
            print(sys.exc_info())
            input()
    

class TryAniFuncAniApp(App):

    def build(self):
        return TryAniFuncAni()

if __name__ == '__main__':
    TryAniFuncAniApp().run()
    
    